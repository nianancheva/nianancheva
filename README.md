# **Hello Internet users!!!**
#### Welcome to my ✨*special*✨ repository.
> Feel free to complain about my horrible writing skills & weird behaviour :)


## **A quick intro:**
- Name: **Nia**
- Surname: **Nancheva**
- Current location: **Europe** (yep, i won't tell the country due to privacy reasons)
- Pronouns: **She/Her**
- Status: **Available for hire**
- Occupation: **Student/Unoccupied**
- Weight: **Hah u wish**

## **Education:**
- **Software University** (SoftUni)
    - **Software Engineering**
    - **Web Development**
    - (January 2021 - Present) 
- **Telerik Academy**
    - **Algorithmic programming**
    - **Game development**
    - (2017 - 2019)
- **"Thomas Jefferson" English High school**
  - **English**
  - **German, Mathematics, Physics and Astronomy**
- **"Friedrich Schiller" Association - Primary School**
  - **German**

## **Some basic info:**

### 👽 **I mostly use these beauties:**
- **`Java`**
- **`JavaScript`** (well, I'm still hopelessly coping with it cuz it's kinda thorny)
### 🐲 **Also slightly experienced with:**
- **`C++`** (before completely dumping it after 2 years of struggle lol)
- **`C#`** (ummm, at least I know how to pronounce it)
- **`SQL`**
- **`PHP`**
- **`Python`** (on the verge of actually starting to learn it)
- **`JSON`** (idk what has happened to jdaughter)
- **`R`** (quite bizarre syntax though)
- **`HTML`**
- **`CSS`**
# <!-- blank line -->

<p align=center>
  <img src="https://user-images.githubusercontent.com/68066820/139449313-a5b56cf7-ad68-48aa-aa3a-0f854bf7affe.png" width=85%/>
</p>
<p align=center>
  (Click on the image for a full-screen view)
</p>
<!-- <img src="https://user-images.githubusercontent.com/68066820/139449313-a5b56cf7-ad68-48aa-aa3a-0f854bf7affe.png"> -->

# <!-- blank line -->
### 🌸 **How to reach me:**
- [**Email**](mailto:niaplnan.business@gmail.com)
- [**Discord**](https://discordapp.com/users/719788587456921601)
- [**Instagram**](https://instagram.com/nianancheva)
- [**Facebook**](https://facebook.com/niaplnan)
### 🍁**Proficiency in languages:**
- **English** (Ehh not the worst)
- **German** (I swear I'm giving it my best)
- **Bulgarian** (It's ok as long as don't have to write lol)
- **Some other langs that don't matter**
> ### 🐸 **Some more time-wasting nonsense:**
> - 🤍 Currently seeking for an internship (and maybe a job hehe)
> - 💬 Ask me about whatever comes to ur mind (I'll instantly become ur best friend if u ask "Wanna us to employ u, kiddo?")
> - 🍭 Stupid fact: I am a vegetarian. Yep, you read it right.
> - 🐋 Fun fact: I used to code on C++ a couple of years ago. But guess what... I fully forgot its syntax...

<br>

```diff
Just a part of one of my fav songs that gives good vibes kk:

@@ B!tch ich bin böse @@
- Nicht böse Hexe
+ Keine böse Fee
! Böse für die Besten!

# (Lieben wir by Shirin David)
```
###### (mhm... i used the template in case u couldn't tell...)
